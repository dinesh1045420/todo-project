import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import { WelcomeDataService } from '../service/data/welcome-data.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  welcomeMessage: string;


  getWelcomeMessage() {
    console.log(this.service.executeHelloWorldBeanService());
    this.service.executeHelloWorldBeanService().subscribe(
      Response => this.handleSuccessfulResponse(Response),
      error => this.handleErrorResponse(error)
    );

    // console.log("Get welcome message");
  }
  handleErrorResponse(error): void {
    this.welcomeMessage = error.error.message;
  }
  handleSuccessfulResponse(Response) {

    this.welcomeMessage = Response.message;
    console.log(Response.message);
  }

  name: any;

  constructor(private router: ActivatedRoute, private service: WelcomeDataService) {

  }

  ngOnInit() {
    this.name = this.router.snapshot.params['name'];
  }


  getWelcomeMessageWithParameter() {
    console.log(this.service.executeHelloWorldBeanService());
    this.service.executeHelloWorldBeanServiceWithPathVariable(this.name).subscribe(
      Response => this.handleSuccessfulResponse(Response),
      error => this.handleErrorResponse(error)
    );

    // console.log("Get welcome message");
  }


}
