import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Router } from '@angular/router';

export class Todo {

  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date
  ) {

  }
}




@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.css']
})
export class ListTodosComponent implements OnInit {


  todos: Todo[];
  message : string ;
  constructor(private todoService : TodoDataService,
    private router : Router){

  }

  ngOnInit(): void {
   this. refereshTodos();
   
    }

    refereshTodos(){
      this.todoService.retriveAllTodos('Dinesh').subscribe(
        response => {
            console.log(response);
            this.todos = response;
        }
      );

    }

 
  //= [
  //   new Todo(1,'Learn to Dance',false,new Date()),
  //   new Todo(2,'Become an expert in Angular ',false,new Date()),
  //   new Todo(2,'visit India ',false,new Date()),

  // ]

  DeleteTodo(id){
    console.log("Delete Todo" + id);
    this.todoService.deleteTodo('Dinesh' , id).subscribe(
      response => { console.log(response);
      this.message =  `Delete of Todo ${id} Successful`;
      this.refereshTodos();
      }
    )
  }

  UpdateTodo(id){
    console.log("Update Todo" + id);
    this.router.navigate(['todos',id]);
    // this.todoService.deleteTodo('Dinesh' , id).subscribe(
    //   response => { console.log(response);
    //   this.message =  `Delete of Todo ${id} Successful`;
    //   this.refereshTodos();
    //   }
    // )
  }
  

}
