import { Component } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import { HardcodedAuthenticationService } from '../service/hardcoded-authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  username: any;
  password: any;
  errorMessage: any = "Invalid credentials";
  invalidLogin = false;

  constructor(private router: Router, public hardcodedAuthentication: HardcodedAuthenticationService) {

  }

  click() {



    if (this.hardcodedAuthentication.authenticate(this.username, this.password)) {
      this.router.navigate(['welcome', this.username]);
      this.invalidLogin = false
    } else {
      this.invalidLogin = true
    }

  }

}
