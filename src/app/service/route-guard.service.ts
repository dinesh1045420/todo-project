import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { HardcodedAuthenticationService } from './hardcoded-authentication.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate  {
currentuser:any;
  constructor(private hardcodedAuthenticationService: HardcodedAuthenticationService,private router:Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  this.currentuser = this.hardcodedAuthenticationService. isUserLoggedIn();  
    
    console.log(this.currentuser);  
     console.log("hi");
    if (this.hardcodedAuthenticationService.isUserLoggedIn) {
    return true;
   }  
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }}); 
      return false;
}
    
}
  



