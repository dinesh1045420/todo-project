import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Todo } from '../list-todos/list-todos.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent  implements OnInit{

  id : number;
  todos : Todo;

  constructor(private todo  : TodoDataService,
    private  route:ActivatedRoute){

  }

  ngOnInit(): void {

    this.id = this.route.snapshot.params['id'];
    this.todos = new Todo(1,"",false,new Date());
    this.todo.getTodo('Dinesh' , this.id).subscribe(
    
        Response=>  this.todos = Response
    
    )
  }

  

  saveTodo(){
    console.log("Upate Todo");
  
  }

}
